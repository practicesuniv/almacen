package Controller;

import Model.Log;
import Model.Product;
import Model.Requests;
import almacen.View.JFrameEditProduct;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import javax.swing.JOptionPane;

public class EditProductController implements ActionListener {

    private JFrameEditProduct jFrameEditProduct = new JFrameEditProduct();
    private Requests rq = new Requests();
    private int id;
    private Product product;

    public EditProductController(JFrameEditProduct jFrameAddProd, int id, Product prod) {
        this.jFrameEditProduct = jFrameAddProd;
        this.id = id;
        this.product = prod;
    }

    public void initListeners() {
        this.jFrameEditProduct.jButtonSave.addActionListener(this);
        this.jFrameEditProduct.jButtonCancel.addActionListener(this);
    }
    
    public void initTextFields() {
        this.jFrameEditProduct.jTextFieldName.setText(this.product.getName());
        this.jFrameEditProduct.jTextFieldQuantity.setText(this.product.getQuantity().toString());
        this.jFrameEditProduct.jTextFieldPrice.setText(this.product.getPrice().toString());
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == jFrameEditProduct.jButtonSave) {
            updateProduct();
        }
        if (e.getSource() == jFrameEditProduct.jButtonCancel) {
            this.jFrameEditProduct.dispose();
        }
    }

    public void updateProduct() {
        if (!"".equals(this.jFrameEditProduct.jTextFieldPrice.getText()) && !"".equals(this.jFrameEditProduct.jTextFieldName.getText()) && !"".equals(this.jFrameEditProduct.jTextFieldQuantity.getText())) {
            Product p = new Product();
            p.setName(this.jFrameEditProduct.jTextFieldName.getText());
            if (this.isInteger(this.jFrameEditProduct.jTextFieldQuantity.getText()) == true) {
                p.setQuantity(Integer.parseInt(this.jFrameEditProduct.jTextFieldQuantity.getText()));
                if (this.isDouble(this.jFrameEditProduct.jTextFieldPrice.getText()) == true) {
                    p.setPrice(Double.parseDouble(this.jFrameEditProduct.jTextFieldPrice.getText()));

                    int r = rq.updateProduct(p, id);
                    
                    Log l = new Log();
                    l.setId_product(this.id);
                    l.setMovement("Actualización");
                    l.setDescription("Se Actualizó el producto: "+this.product.getName());
                    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
                    l.setMovDate(dtf.format(LocalDateTime.now()));
                    
                    if (r == 1) {
                        JOptionPane.showMessageDialog(this.jFrameEditProduct, "Producto actualizado con éxito.");
                        rq.insertLog(l);
                    } else {
                        JOptionPane.showMessageDialog(this.jFrameEditProduct, "Error");
                    }
                    this.jFrameEditProduct.dispose();
                } else {
                    JOptionPane.showMessageDialog(this.jFrameEditProduct, "El precio debe de ser un numero");
                }
            } else {
                JOptionPane.showMessageDialog(this.jFrameEditProduct, "La cantidad debe de ser un numero");
            }
        } else {
            JOptionPane.showMessageDialog(this.jFrameEditProduct, "Debes completar todos los campos");
        }
    }

    private boolean isInteger(String s) {
        boolean r;
        try {
            Integer.parseInt(s);
            r = true;
        } catch (NumberFormatException e) {
            r = false;
        }
        return r;
    }

    private boolean isDouble(String s) {
        boolean r;
        try {
            Double.parseDouble(s);
            r = true;
        } catch (NumberFormatException e) {
            r = false;
        }
        return r;
    }
}
