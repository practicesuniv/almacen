package Controller;

import Model.Log;
import Model.Password;
import Model.Product;
import Model.Requests;
import almacen.View.JFrameAddProduct;
import almacen.View.JFrameEditProduct;
import almacen.View.JFrameHome;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;

public class HomeController implements ActionListener {

    private JFrameHome jFrameHome = new JFrameHome();
    private DefaultTableModel model = new DefaultTableModel();
    private Requests rq = new Requests();
    private JFrameAddProduct jFrameAddProduct;
    private JFrameEditProduct jFrameEditProduct;

    public HomeController(JFrameHome jframeHome) {
        this.jFrameHome = jframeHome;
    }

    public void initListeners() throws SQLException {
        this.jFrameHome.jButtonAddProduct.addActionListener(this);
        this.jFrameHome.jButtonDelete.addActionListener(this);
        this.jFrameHome.jButtonDiscount.addActionListener(this);
        this.jFrameHome.jButtonEditProducto.addActionListener(this);
        this.jFrameHome.jButtonSearch.addActionListener(this);
        this.jFrameHome.jButtonSearchMov.addActionListener(this);
        this.jFrameHome.jButtonDeleteLogs.addActionListener(this);
        this.jFrameHome.jRadioButtonShowAllMovs.addActionListener(this);
        this.jFrameHome.jRadioButtonShowMovsProductSelected.addActionListener(this);
        this.initJtable();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // Eventos de crear ventana agregar producto
        if (e.getSource() == jFrameHome.jButtonAddProduct) {
            this.initJFrameAddProduct();
        }

        // Eventos de crear ventana editar producto
        if (e.getSource() == jFrameHome.jButtonEditProducto) {
            this.initJFrameEditProduct();
        }

        // Eliminar Producto Seleccionado
        if (e.getSource() == jFrameHome.jButtonDelete) {
            try {
                this.deleteProduct();
            } catch (SQLException ex) {
                Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        // Evento de click en botón buscar
        if (e.getSource() == jFrameHome.jButtonSearch) {
            try {
                this.searchProduct();
            } catch (SQLException ex) {
                Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        // Evento de click en boton descontar producto
        if (e.getSource() == jFrameHome.jButtonDiscount) {
            try {
                this.discount();
            } catch (SQLException ex) {
                Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        // Evento de click en boton borrar todos los logs
        if (e.getSource() == jFrameHome.jButtonDeleteLogs) {
            try {
                this.deleteLogs();
            } catch (SQLException ex) {
                Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (e.getSource() == jFrameHome.jButtonSearchMov) {
            try {
                this.searchLog();
            } catch (SQLException ex) {
                Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (e.getSource() == jFrameHome.jRadioButtonShowAllMovs) {
            this.showAllLogs();
        }

        if (e.getSource() == jFrameHome.jRadioButtonShowMovsProductSelected) {
            this.showLogsProductSelected();
        }
    }

    public void initJFrameAddProduct() {
        this.jFrameAddProduct = new JFrameAddProduct();
        this.jFrameAddProduct.setLocationRelativeTo(null);
        this.jFrameAddProduct.setVisible(true);
        AddProductController addCtrl = new AddProductController(this.jFrameAddProduct);
        addCtrl.initListeners();

        jFrameAddProduct.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosed(java.awt.event.WindowEvent windowEvent) {
                try {
                    initJtable();
                } catch (SQLException ex) {
                    Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    public void initJFrameEditProduct() {
        int row = this.jFrameHome.jTableProducts.getSelectedRow();
        if (row >= 0) {
            int id = Integer.parseInt(this.jFrameHome.jTableProducts.getValueAt(row, 0).toString());
            this.jFrameEditProduct = new JFrameEditProduct();
            this.jFrameEditProduct.setLocationRelativeTo(null);
            this.jFrameEditProduct.setVisible(true);
            EditProductController editCtrl = new EditProductController(this.jFrameEditProduct, id, this.productSelected());
            editCtrl.initListeners();
            editCtrl.initTextFields();

            jFrameEditProduct.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosed(java.awt.event.WindowEvent windowEvent) {
                    try {
                        initJtable();
                    } catch (SQLException ex) {
                        Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
        } else {
            JOptionPane.showMessageDialog(this.jFrameHome, "Debe seleccionar un producto primero para luego editarlo");
        }
    }

    public void initJtable() throws SQLException {
        ResultSet a = rq.getProduct();
        model = (DefaultTableModel) this.jFrameHome.jTableProducts.getModel();
        model.setRowCount(0);
        this.jFrameHome.jTableProducts.setModel(model);
        while (a.next()) {
            model.addRow(new Object[]{a.getInt("id"), a.getString("name"), a.getString("quantity"), a.getString("price")});
        }

        this.jFrameHome.jRadioButtonShowAllMovs.setSelected(true);
        this.jFrameHome.jRadioButtonShowMovsProductSelected.setEnabled(false);
        
        model = (DefaultTableModel) this.jFrameHome.jTableMovs.getModel();
        model.setRowCount(0);
        this.jFrameHome.jTableMovs.setModel(model);
        this.showAllLogs();

        this.jFrameHome.jTableProducts.getColumnModel().getColumn(0).setMinWidth(0);
        this.jFrameHome.jTableProducts.getColumnModel().getColumn(0).setMaxWidth(0);
        this.jFrameHome.jTableMovs.getColumnModel().getColumn(0).setMinWidth(0);
        this.jFrameHome.jTableMovs.getColumnModel().getColumn(0).setMaxWidth(0);
        this.jFrameHome.jTableMovs.getColumnModel().getColumn(1).setMinWidth(0);
        this.jFrameHome.jTableMovs.getColumnModel().getColumn(1).setMaxWidth(0);
        this.jFrameHome.jTableMovs.getColumnModel().getColumn(4).setMinWidth(250);
    }

    public void searchProduct() throws SQLException {
        ResultSet a = rq.searchProduct(this.jFrameHome.jTextFieldSearch.getText());
        model = (DefaultTableModel) this.jFrameHome.jTableProducts.getModel();
        this.jFrameHome.jTableProducts.setModel(model);
        model.setRowCount(0);
        while (a.next()) {
            model.addRow(new Object[]{a.getInt("id"), a.getString("name"), a.getString("quantity"), a.getString("price")});
        }
    }

    public void discount() throws SQLException {
        int row = this.jFrameHome.jTableProducts.getSelectedRow();
        Log l = new Log();
        if (row >= 0) {
            if (Integer.parseInt(this.jFrameHome.jTableProducts.getValueAt(row, 2).toString()) > 0) {
                int r = rq.discountProduct((int) this.jFrameHome.jTableProducts.getValueAt(row, 0));
                l.setId_product((int) this.jFrameHome.jTableProducts.getValueAt(row, 0));
                l.setMovement("Descuento de Unidad");
                l.setDescription("Se Descontó un producto de: " + this.jFrameHome.jTableProducts.getValueAt(row, 1));
                if (r == 1) {
                    JOptionPane.showMessageDialog(this.jFrameHome, "Cantidad Actualizada con Exito.");
                    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
                    l.setMovDate(dtf.format(LocalDateTime.now()));
                    rq.insertLog(l);
                    model = (DefaultTableModel) this.jFrameHome.jTableProducts.getModel();
                    this.jFrameHome.jTableProducts.setModel(model);
                    model.setRowCount(0);
                    this.initJtable();
                } else {
                    JOptionPane.showMessageDialog(this.jFrameHome, "Error");
                }
            } else {
                JOptionPane.showMessageDialog(this.jFrameHome, "No tienes Stock de este producto!");
            }
        } else {
            JOptionPane.showMessageDialog(this.jFrameHome, "Debe seleccionar un producto primero para descontarle una unidad");
        }
    }

    public void deleteProduct() throws SQLException {
        int row = this.jFrameHome.jTableProducts.getSelectedRow();
        Log l = new Log();
        l.setMovement("Eliminación de Producto");
        l.setDescription("Se Eliminó el producto: " + this.jFrameHome.jTableProducts.getValueAt(row, 1));
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        l.setMovDate(dtf.format(LocalDateTime.now()));
        if (row >= 0) {
            rq.deleteProduct((int) this.jFrameHome.jTableProducts.getValueAt(row, 0));
            rq.insertLog(l);
            model = (DefaultTableModel) this.jFrameHome.jTableProducts.getModel();
            this.jFrameHome.jTableProducts.setModel(model);
            model.setRowCount(0);
            this.initJtable();
        } else {
            JOptionPane.showMessageDialog(this.jFrameHome, "Debe seleccionar un producto primero para eliminarlo");
        }
    }

    public void deleteLogs() throws SQLException {
        Password p = new Password();
            String s = JOptionPane.showInputDialog("Ingrese la contraseña Maestra: ");
            if(s != null){
                 if(s.equals(p.getPass())){
                    int n = JOptionPane.showConfirmDialog(null, "Esta seguro que quiere realizar esta acción?. Se borrarán todos los logs.", "Confirmación", JOptionPane.YES_NO_OPTION);
                    if(n == 0){
                            rq.deleteLogs();
                            this.initJtable();
                    }else{
                        System.out.println("No");
                    }
                }else{
                    JOptionPane.showMessageDialog(this.jFrameHome, "Contraseña incorrecata");
                }
            }
    }

    public Product productSelected() {
        int row = this.jFrameHome.jTableProducts.getSelectedRow();
        if (row >= 0) {
            Product product = new Product();
            product.setId(Integer.parseInt(this.jFrameHome.jTableProducts.getValueAt(row, 0).toString()));
            product.setName((String) this.jFrameHome.jTableProducts.getValueAt(row, 1));
            product.setQuantity(Integer.parseInt(this.jFrameHome.jTableProducts.getValueAt(row, 2).toString()));
            product.setPrice((Double.parseDouble(this.jFrameHome.jTableProducts.getValueAt(row, 3).toString())));

            return product;
        }
        return null;
    }

    public void searchLog() throws SQLException {
        ResultSet a = rq.searchLog(this.jFrameHome.jTextFieldSearchMovs.getText());
        model = (DefaultTableModel) this.jFrameHome.jTableMovs.getModel();
        this.jFrameHome.jTableMovs.setModel(model);
        model.setRowCount(0);
        while (a.next()) {
            model.addRow(new Object[]{a.getInt("id"), a.getInt("id_product"), a.getString("movement"), a.getString("date"), a.getString("description")});
        }
    }

    public void showLogsProductSelected() {
        int row = this.jFrameHome.jTableProducts.getSelectedRow();
        Log l = new Log();
        if (row >= 0) {
            ResultSet b = rq.getLogs((int) this.jFrameHome.jTableProducts.getValueAt(row, 0));
            model = (DefaultTableModel) this.jFrameHome.jTableMovs.getModel();
            model.setRowCount(0);
            this.jFrameHome.jTableMovs.setModel(model);
            try {
                while (b.next()) {
                    model.addRow(new Object[]{b.getInt("id"), b.getInt("id_product"), b.getString("movement"), b.getString("date"), b.getString("description")});
                }
            } catch (SQLException ex) {
                Logger.getLogger(JFrameHome.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void showAllLogs() {
        Log l = new Log();
        ResultSet b = rq.getLogs();
        model = (DefaultTableModel) this.jFrameHome.jTableMovs.getModel();
        model.setRowCount(0);
        this.jFrameHome.jTableMovs.setModel(model);
        try {
            while (b.next()) {
                model.addRow(new Object[]{b.getInt("id"), b.getInt("id_product"), b.getString("movement"), b.getString("date"), b.getString("description")});
            }
        } catch (SQLException ex) {
            Logger.getLogger(JFrameHome.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
