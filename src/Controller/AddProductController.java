package Controller;

import Model.Log;
import Model.Product;
import Model.Requests;
import almacen.View.JFrameAddProduct;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import javax.swing.JOptionPane;

public class AddProductController implements ActionListener {

    private JFrameAddProduct jFrameAddProduct = new JFrameAddProduct();
    private Requests rq = new Requests();

    public AddProductController(JFrameAddProduct jFrameAddProd) {
        this.jFrameAddProduct = jFrameAddProd;
    }

    public void initListeners() {
        this.jFrameAddProduct.jButtonAdd.addActionListener(this);
        this.jFrameAddProduct.jButtonCancel.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == jFrameAddProduct.jButtonAdd) {
            // Llamar metodo de crear producto
            this.insertProduct();
        }
        if (e.getSource() == jFrameAddProduct.jButtonCancel) {
            this.jFrameAddProduct.dispose();
        }
    }

    public void insertProduct() {
        if ("".equals(this.jFrameAddProduct.jTextFieldName.getText()) || "".equals(this.jFrameAddProduct.jTextFieldQuantity.getText()) || "".equals(this.jFrameAddProduct.jTextFieldPrice.getText())) {
            JOptionPane.showMessageDialog(this.jFrameAddProduct, "Debes completar todos los campos");
        } else {
            Product p = new Product();
            Log l = new Log();
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
            p.setName(this.jFrameAddProduct.jTextFieldName.getText());
            if (this.isInteger(this.jFrameAddProduct.jTextFieldQuantity.getText()) == true) {
                p.setQuantity(Integer.parseInt(this.jFrameAddProduct.jTextFieldQuantity.getText()));
                if (this.isDouble(this.jFrameAddProduct.jTextFieldPrice.getText()) == true) {
                    l.setMovement("Producto Añadido");
                    l.setDescription("El producto añadido es: "+this.jFrameAddProduct.jTextFieldName.getText());
                    
                    l.setMovDate(dtf.format(LocalDateTime.now()));
                    p.setPrice(Double.parseDouble(this.jFrameAddProduct.jTextFieldPrice.getText()));
                    rq.insertProduct(p);
                    rq.insertLog(l);
                    this.jFrameAddProduct.dispose();
                } else {
                    JOptionPane.showMessageDialog(this.jFrameAddProduct, "El precio debe de ser un numero");
                }
            } else {
                JOptionPane.showMessageDialog(this.jFrameAddProduct, "La cantidad debe de ser un numero");
            }
        }
    }

    private boolean isInteger(String s) {
        boolean r;
        try {
            Integer.parseInt(s);
            r = true;
        } catch (NumberFormatException e) {
            r = false;
        }
        return r;
    }

    private boolean isDouble(String s) {
        boolean r;
        try {
            Double.parseDouble(s);
            r = true;
        } catch (NumberFormatException e) {
            r = false;
        }
        return r;
    }

}
