package Model;

import java.sql.Date;

public class Log {
    private Integer id;
    private Integer id_product;
    private String Movement;
    private String movDate;
    private String description;

    public Log() {
    }

    public Log(Integer id, Integer id_product, String Movement, String movDate, String description) {
        this.id = id;
        this.id_product = id_product;
        this.Movement = Movement;
        this.movDate = movDate;
        this.description = description;
    }   
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId_product() {
        return id_product;
    }

    public void setId_product(Integer id_product) {
        this.id_product = id_product;
    }

    public String getMovement() {
        return Movement;
    }

    public void setMovement(String Movement) {
        this.Movement = Movement;
    }

    public String getMovDate() {
        return movDate;
    }

    public void setMovDate(String movDate) {
        this.movDate = movDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
            
}
