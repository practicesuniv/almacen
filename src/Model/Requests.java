package Model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Requests {

    DBConnection dbcon = new DBConnection();

    public Requests() {

    }

    public ResultSet getProduct() {
        ResultSet result = null;
        try {
            PreparedStatement st = dbcon.getConnection().prepareStatement("SELECT * FROM product");
            result = st.executeQuery();
            return result;
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }

    public ResultSet searchProduct(String name) {
        ResultSet result = null;
        try {
            PreparedStatement st = dbcon.getConnection().prepareStatement("SELECT * FROM product WHERE name LIKE '%" + name + "%'");
            result = st.executeQuery();
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(Requests.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int discountProduct(int id) {
        int r = 0;
        try {
            PreparedStatement st = dbcon.getConnection().prepareStatement("UPDATE product SET quantity= quantity-'1' WHERE id='" + id + "'");
            r = st.executeUpdate();
            return r;

        } catch (SQLException ex) {
            Logger.getLogger(Requests.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    public void deleteProduct(int id) throws SQLException {
        PreparedStatement st = dbcon.getConnection().prepareStatement("DELETE FROM product WHERE id='" + id + "'");
        st.executeUpdate();
        
        int r = 0;
        try {
            st = dbcon.getConnection().prepareStatement("UPDATE log SET id_product= 'null' WHERE id_product='" + id + "'");
            r = st.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Requests.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void insertProduct(Product p) {
        try {
            PreparedStatement st = dbcon.getConnection().prepareStatement("INSERT INTO product VALUES(null, '" + p.getName() + "','" + p.getQuantity() + "','" + p.getPrice() + "')");
            st.execute();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public int updateProduct(Product p, int id) {
        int r = 0;
        try {
            PreparedStatement st = dbcon.getConnection()
                    .prepareStatement("UPDATE product SET "
                            + "name = '" + p.getName() + "', "
                            + "quantity = '" + p.getQuantity()
                            + "', price = '" + p.getPrice()
                            + "' WHERE id='" + id + "'");
            r = st.executeUpdate();
            return r;

        } catch (SQLException ex) {
            Logger.getLogger(Requests.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }
    
    public void insertLog(Log log){
        try {
            PreparedStatement st = dbcon.getConnection().prepareStatement("INSERT INTO log VALUES(null, '" + log.getId_product() + "','" + log.getMovement() + " ','"+ log.getMovDate() +"','"+ log.getDescription() + "')");
            st.execute();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }
    
    public ResultSet getLogs(int id) {
        ResultSet result = null;
        try {
            PreparedStatement st = dbcon.getConnection().prepareStatement("SELECT * FROM log WHERE id_product ="+id);
            result = st.executeQuery();
            return result;
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }
    
    public ResultSet getLogs() {
        ResultSet result = null;
        try {
            PreparedStatement st = dbcon.getConnection().prepareStatement("SELECT * FROM log");
            result = st.executeQuery();
            return result;
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }
    
     public void deleteLogs() throws SQLException {
        PreparedStatement st = dbcon.getConnection().prepareStatement("DELETE FROM log");
        st.executeUpdate();
    }
    
     public ResultSet searchLog(String description) {
        ResultSet result = null;
        try {
            PreparedStatement st = dbcon.getConnection().prepareStatement("SELECT * FROM log WHERE description LIKE '%" + description + "%'");
            result = st.executeQuery();
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(Requests.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
