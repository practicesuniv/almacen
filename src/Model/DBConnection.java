package Model;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBConnection {

    private Connection con = null;
    private Statement stment;

    public DBConnection(){
        
    }
    
    public Connection getConnection() {
        try {
            if (con == null) {
                Class.forName("org.sqlite.JDBC");
                con = DriverManager.getConnection("jdbc:sqlite:despensa.db");
                stment = (Statement) con.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);               
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return con;
    }

    public Statement getStment() {
        return stment;
    }

    public void setStment(Statement stment) {
        this.stment = stment;
    }
    
    
}
